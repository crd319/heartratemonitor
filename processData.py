import csv
import math
import numpy as np
from pylab import *
from scipy.signal import butter,lfilter

def butter_bandpass(lowcut, highcut, fs, order=5):
	nyq = 0.5 * fs
	low = lowcut / nyq
	high = highcut / nyq
	b, a = butter(order, [low, high], btype='band')
	return b, a

def butter_lowpass(cutoff, fs, order=5):
	nyq = 0.5 * fs
	normal = cutoff / nyq
	b, a = butter(order, normal, btype='low', analog=False)
	return b, a

def butter_bandpass_filter(data, lowcut, highcut, fs, order=5):
	b, a = butter_bandpass(lowcut, highcut, fs, order=order)

	y = lfilter(b, a, data)
	return y

def butter_lowpass_filter(data, cutoff, fs, order=5):
	b, a = butter_lowpass(cutoff, fs, order=order)

	y = lfilter(b, a, data)
	return y

def resample(data, rate):
	freq = 0.02
	time = 0 + freq
	offset = 0
	i = 1
	arrSize =  size(data[:,0])
	while i < arrSize-1: 
		prevPt = data[i-1]
		cont = True
		while (cont):
			if data[i,0] != time:
				if (i+1 < arrSize):
					if data[i,0] < time and data[i+1,0] < time:
						prevPt = data[i]
						data = np.delete(data, (i), axis=0)
					elif data[i,0] < time and data[i+1,0] == time:
						prevPt = data[i]
						data = np.delete(data, (i), axis=0)
						i = i + 1
						cont=False
					elif data[i,0] < time and data[i+1,0] > time:
						prevPt = data[i]
						data = np.delete(data,(i), axis=0)
						#i = i + 2
					elif data[i,0] > time:
						nextPt = data[i]
						data[i,0] = time
						data[i,1] = prevPt[1]+(nextPt[1]-prevPt[1])*((time-prevPt[0])/(nextPt[0]-prevPt[0]))
						data[i,2] = prevPt[2]+(nextPt[2]-prevPt[2])*((time-prevPt[0])/(nextPt[0]-prevPt[0]))
						data[i,3] = prevPt[3]+(nextPt[3]-prevPt[3])*((time-prevPt[0])/(nextPt[0]-prevPt[0]))
						i=i+1
						cont=False
					arrSize = size(data[:,0])
				else:
					data = np.delete(data, (i), axis=0)
					cont = False
					break;
			arrSize = size(data[:,0])
		time = time + freq
		
	return data

'''
list = ['./Run2/Rest/gyroscope.csv','./Run2/1mph/gyroscope.csv', './Run2/1_5mph/gyroscope.csv', \
	'./Run2/2mph/gyroscope.csv', './Run2/2_5mph/gyroscope.csv', './Run2/3mph/gyroscope.csv',\
	'./Run2/3_5mph/gyroscope.csv', './Run2/4mph/gyroscope.csv']
'''

'''
list = ['./Run3/Rest/gyroscope.csv','./Run3/1mph/gyroscope.csv', './Run3/1_5mph/gyroscope.csv', \
	'./Run3/2mph/gyroscope.csv', './Run3/2_5mph/gyroscope.csv', './Run3/3mph/gyroscope.csv',\
	'./Run3/3_5mph/gyroscope.csv', './Run3/4mph/gyroscope.csv']
'''
'''
list =  ['./Run4/Rest/gyroscope.csv','./Run4/1mph/gyroscope.csv', './Run4/1_5mph/gyroscope.csv', \
	'./Run4/2mph/gyroscope.csv', './Run4/2_5mph/accelerometer.csv', './Run4/3mph/gyroscope.csv',\
	'./Run4/3_5mph/gyroscope.csv', './Run4/4mph/gyroscope.csv']
'''

list = ['./Run4/1mph/gyroscope.csv']

for file in list:
	arr = [line.split(',') for line in open(file)]
	t0 = float(arr[0][0])
	cutoff = 20

	arr = np.asarray(arr)
	data = arr.astype(np.float)
	dataSampled = data

	x = [(t-t0)/1000.0 for t in data[:,0]]
	data[:,0] = x
	dataSampled = resample(data, 50)

	
	figure(1)
	plot(dataSampled[10:250,0], dataSampled[10:250,1], 'r', label='x')
	plot(dataSampled[10:250,0], dataSampled[10:250,2], 'g', label='y')
	plot(dataSampled[10:250,0], dataSampled[10:250,3], 'b', label='z')
	legend(loc=1)
	xlabel('Time(sec)')
	
	lowpassX = butter_lowpass_filter(dataSampled[:, 1], cutoff,  50, order=3);
	lowpassY = butter_lowpass_filter(dataSampled[:, 2], cutoff,  50, order=3);
	lowpassZ = butter_lowpass_filter(dataSampled[:, 3], cutoff,  50, order=3);
	
	figure(2)
	plot(dataSampled[10:250,0], lowpassX[10:250], 'r', label='x')
	plot(dataSampled[10:250,0], lowpassY[10:250], 'g', label='y')
	plot(dataSampled[10:250,0], lowpassZ[10:250], 'b', label='z')
	legend(loc=1)
	xlabel('Time(sec)')
	
	dataSampled[:,1] = dataSampled[:,1]-lowpassX
	dataSampled[:,2] = dataSampled[:,2]-lowpassY
	dataSampled[:,3] = dataSampled[:,3]-lowpassZ
	
	xSD = np.std(dataSampled[:,1])
	ySD = np.std(dataSampled[:,2])
	zSD = np.std(dataSampled[:,3])

	figure(3)
	plot(dataSampled[10:250,0], dataSampled[10:250,1], 'r', label='x')
	plot(dataSampled[10:250,0], dataSampled[10:250,2], 'g', label='y')
	plot(dataSampled[10:250,0], dataSampled[10:250,3], 'b', label='z')
	legend(loc=1)
	xlabel('Time(sec)')
	
	dataSampled[abs(dataSampled[:,1]) > (2*xSD),1] = 0
	dataSampled[abs(dataSampled[:,2]) > (1*ySD),2] = 0
	dataSampled[abs(dataSampled[:,3]) > (2*zSD),3] = 0
	
	figure(4)
	plot(dataSampled[10:250,0], dataSampled[10:250,1], 'r', label='x')
	plot(dataSampled[10:250,0], dataSampled[10:250,2], 'g', label='y')
	plot(dataSampled[10:250,0], dataSampled[10:250,3], 'b', label='z')
	legend(loc=1)
	xlabel('Time(sec)')
	

	for i in range(1, size(dataSampled[:,0])-1):
		dataSampled[i,1] = (dataSampled[i,1]-(dataSampled[i-1,1]+dataSampled[i,1]+dataSampled[i+1,1])/3.0)
		dataSampled[i,2] = (dataSampled[i,2]-(dataSampled[i-1,2]+dataSampled[i,2]+dataSampled[i+1,2])/3.0)
		dataSampled[i,3] = (dataSampled[i,3]-(dataSampled[i-1,3]+dataSampled[i,3]+dataSampled[i+1,3])/3.0)


	xFiltered = butter_bandpass_filter(dataSampled[10:, 1], 10, 13, 50, order=4)
	yFiltered = butter_bandpass_filter(dataSampled[10:, 2], 10, 13, 50, order=4)
	zFiltered = butter_bandpass_filter(dataSampled[10:, 3], 10, 13, 50, order=4)

	figure(5)
	plot(dataSampled[0:250,0], xFiltered[0:250], 'r')
	plot(dataSampled[0:250,0], yFiltered[0:250], 'g')
	plot(dataSampled[0:250,0], zFiltered[0:250], 'b')
	legend(loc=1)
	xlabel('Time(sec)')
	
	
	BCG = [math.sqrt( math.pow(x,2)+math.pow(y,2)+math.pow(z,2) ) for(x,y,z) in zip(xFiltered, yFiltered, zFiltered)]
	
	Pulse = butter_bandpass_filter(BCG, 0.75, 2.5, 50, order=2)
	
	
	figure(6)
	plot(dataSampled[10:250,0],abs(Pulse[0:240]))
	xlabel('Time(sec)')
	

	n = 0
	i = 0
	total = 0 
	print file
	while n+250 < len(Pulse):
		F = np.fft.fft((Pulse[n:n+200]))
		freq = np.fft.fftfreq(len(F))
		Fx = abs(freq*50)
		
		
		idx = np.argmax(abs(F[0:150]))
		fq = Fx[idx]

		if(fq*60) >= 60:
			#print n/50.0, (n+250)/50.0		
			#print fq*60
		
			total = total+(fq*60)
			i = i + 1
		n = n+50
		
	
	figure(7)
	plot(Fx[0:150], abs(F[0:150]))
	title('Heart Rate: '+str(fq*60))
	xlabel('Frequency(Hz)')
	ylabel('|fft|')
	show()
	
	print 'Avg=', str(total/i)
