package com.chrisdeangelis.heartratemonitor;

import android.app.Activity;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import android.util.Log;

public class MainActivity extends Activity implements SensorEventListener{

    private TextView text, time, status;
    private SensorManager sManager;
    private Sensor sensor;
    private CountDownTimer countdown, execute;
    private static boolean collect = false;
    private static boolean experimentDone = false;
    //private File output;
    private BufferedWriter out;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        text = (TextView) findViewById(R.id.dataStream);
        time =(TextView) findViewById(R.id.timerView);
        status = (TextView) findViewById(R.id.text1);

        sManager =(SensorManager) getSystemService(SENSOR_SERVICE);
        sensor = sManager.getSensorList(Sensor.TYPE_GYROSCOPE).get(0);
        sManager.registerListener(this, sManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE), SensorManager.SENSOR_DELAY_GAME);

        try {
            File baseDIR = Environment.getExternalStorageDirectory();
            //File myFile = new File( Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS), "DataFile");
            //if(!myFile.exists()){
            //   myFile.mkdirs();
            //}
            File outFile = new File("/sdcard/", "Data.csv");
            out = new BufferedWriter(new FileWriter(outFile));
            out.write("Hello, World");

        } catch (Exception e) {
            e.printStackTrace();
        }

        startCountdown();
    }

    @Override
    protected void onStop(){
        sManager.unregisterListener(this);
        super.onStop();
    }

    @Override
    protected void onResume(){
        super.onResume();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        if(event.accuracy == SensorManager.SENSOR_STATUS_UNRELIABLE || !collect){
            return;
        }
        text.setText("Orientation X: "+Float.toString(event.values[2])+"\n"+
                     "Orientation Y: "+Float.toString(event.values[1])+"\n"+
                     "Orientation Z: "+Float.toString(event.values[0])+"\n");
        try {
            out.write(Float.toString(event.values[2]) + "," +
                    Float.toString(event.values[1]) + "," +
                    Float.toString(event.values[0]));
        } catch (IOException e) {
            e.printStackTrace();
        }


    }
    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }

    public void startCountdown(){
        if(!experimentDone) {
            String sTime = time.getText().toString();
            String Minutes = sTime.substring(0, 2);
            String Seconds = sTime.substring(3);
            Long iMin = Long.parseLong(Minutes);
            Long iSec = Long.parseLong(Seconds);

            //Convert hours, minutes, seconds to millisections
            long totalTime = (iMin * 60 + iSec) * 1000;

            countdown = new CountDownTimer(totalTime, 1000) {
                @Override
                public void onTick(long timeToComplete) {

                    //Convert milliseconds to seconds, minutes, hour components.
                    int seconds = (int) (timeToComplete / 1000) % 60;
                    int minutes = (int) ((timeToComplete / (1000 * 60)) % 60);

                    String sSec = String.format("%02d", seconds);
                    String sMin = String.format("%02d", minutes);

                    time.setText(sMin + ":" + sSec);
                }

                @Override
                public void onFinish() {
                    time.setText("00:10");
                    startCollect();
                }
            };
            countdown.start();
        }
    }

    public void startCollect(){
        collect = true;
        status.setText("Execute");
        String sTime = time.getText().toString();
        String Minutes = sTime.substring(0, 2);
        String Seconds = sTime.substring(3);
        Long iMin = Long.parseLong(Minutes);
        Long iSec = Long.parseLong(Seconds);

        //Convert hours, minutes, seconds to millisections
        long totalTime = (iMin * 60 + iSec) * 1000;

        execute = new CountDownTimer(totalTime, 1000) {
            @Override
            public void onTick(long timeToComplete) {

                //Convert milliseconds to seconds, minutes, hour components.
                int seconds = (int) (timeToComplete / 1000) % 60;
                int minutes = (int) ((timeToComplete / (1000 * 60)) % 60);

                String sSec = String.format("%02d", seconds);
                String sMin = String.format("%02d", minutes);

                time.setText(sMin + ":" + sSec);
            }

            @Override
            public void onFinish() {
                time.setText("00:00");
                status.setText("Complete");
                collect = false;
                experimentDone = true;
                try {
                    out.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        };
        execute.start();
    }
}
